//
//  home.swift
//  testMovies
//
//  Created by Bruno Cardenas on 26/02/21.
//

import SwiftUI

struct home: View {
    init() {
            UINavigationBar.appearance().largeTitleTextAttributes = [.foregroundColor: UIColor.white]

        }
    let columns = [
            GridItem(.flexible()),
            GridItem(.flexible())
        ]
    
    @ObservedObject var vm = homeViewModel()
    var body: some View {
        NavigationView {
            ScrollView {
                Spacer(minLength: 80)
                Text("TV SHOWS").bold().font(.largeTitle).foregroundColor(.white)
                CustomPickerView(selectedIndex: self.$vm.selectedIndexCategories)
                    .background(Rectangle().fill(Color.clear))
                LazyVGrid(columns: columns, spacing: 20) {
                    ForEach(self.vm.movies ?? [], id: \.id) { value in
                        movieCard(movie: value)
                            }
                        }
                    }.background(Color.black)
                   

            .navigationBarHidden(true)
            .edgesIgnoringSafeArea([.top, .bottom])

        }
     
    }
}

struct home_Previews: PreviewProvider {
    static var previews: some View {
        home()
    }
}
